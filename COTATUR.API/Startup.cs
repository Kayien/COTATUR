﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(COTATUR.API.Startup))]
namespace COTATUR.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
